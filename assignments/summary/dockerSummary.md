**_DOCKER BASICS_**
1) **Docker** - Docker is a program for developers to develop, and run applications with containers.
2) **Docker Image** - A Docker image is contains everything needed to run an applications as a container. This includes: code, runtime, libraries,environment variables, configuration files. The image can then be deployed to any Docker environment and as a container.
3) **Container** - Docker container is running docker image, from one image you can create multiple containers.
4) **Docker Hub** - Docker hub is a github for docker images and containers

**_DOCKER BASIC Commands_**
1) **_$docker ps_** - allows to view all container that are running in docker host.
2) **_$docker start_** - this command starts any stopped container
3) **_$docker stop_** - stops the running containers
4) **_$docker run_** - creates containers from docker images
5) **_$docker rm_** - deletes the containers

**_DOCKER COMMON OPERATIONS_**
1) **Download the docker** - _$docker pull name/trydock_
2) **Run the docker image** - _$docker run -ti snehabhapkar/trydock /bin/bash_
3) **Copy code inside docker** - docker cp hello.py e0b72ff850f8:/
**hello.py is the code to be Copied**
4) **Install dependencies** -   
**_Give permission to run shell script_**
_docker exec -it e0b72ff850f8 chmod +x requirements.sh_  
**_Install dependencies_**  
_docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh_

5) **Run the program inside the container** -   
_docker start e0b72ff850f8_
docker exec e0b72ff850f8 python3 hello.py_

6) **Save your copied code inside docker image** - _docker commit e0b72ff850f8 snehabhapkar/trydock_
7) **Push to dockerhub** -   
**_Tag image name with different name_**
_docker tag snehabhapkar/trydock username/repo_  
username is username on dockerhub and repo is name of image

**Push on dockerhub**
_docker push username/repo_
